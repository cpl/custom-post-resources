<?php

namespace cpl\custom_post_resources;
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://skorasaur.us
 * @since             1.0.1
 * @package           Custom_Post_Resources
 *
 * @wordpress-plugin
 * Plugin Name:       CPL Custom Post Resources
 * Plugin URI:        https://gitlab.com/cpl/custom-post-resources
 * Description:       A theme-agnostic WP plugin for Custom post type for resources
 *                    It is largely suited for CPL use but is adaptable.
 *                    It requires Advanced Custom Fields Pro
 *                    and Gutenberg.
 * Requires PHP:      7.3+
 * Version:           0.0.4
 * Author:            will skora
 * Author URI:        https://skorasaur.us
 * License:           GPL-3.0+
 * License URI:       https://choosealicense.com/licenses/gpl-3.0/
 * Text Domain:       cpl_domain_cpr
 * Domain Path:       /languages
 */


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// not sure if this a standard?
define( 'cpl_version_cpr', '0.0.1' );
define( 'cpl_domain_cpr', 'custom-post-resources' );
define( 'cpl_path_cpr', plugin_dir_path( __FILE__ ) );


require_once( cpl_path_cpr . '/post-types/register.php' );
require_once( cpl_path_cpr . '/register-taxonomy.php' );

add_action( 'init', __NAMESPACE__ . '\cp_register_resource' );


add_action( 'init', __NAMESPACE__ . '\register_cpl_resource_subject', 0 );


// https://codex.wordpress.org/Function_Reference/register_post_type#Flushing_Rewrite_on_Activation
// This is to flush rewrites so if I decide to change the path of the CPT, it will auotmatically
// do it on plugin activation
function my_rewrite_flush() {
	// First, we "add" the custom post type via the above written function.
	// Note: "add" is written with quotes, as CPTs don't get added to the DB,
	// They are only referenced in the post_type column with a post entry,
	// when you add a post of this CPT.
	cp_register_resource();

	// ATTENTION: This is *only* done during plugin activation hook in this example!
	// You should *NEVER EVER* do this on every page load!!
	\flush_rewrite_rules();
}
register_activation_hook( __FILE__, __NAMESPACE__ . '\my_rewrite_flush' );
