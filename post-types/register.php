<?php

namespace cpl\custom_post_resources;

// Register Custom Post Type
function cp_register_resource() {

	$labels = array(
		'name'                  => _x( 'E-Resources', 'Post Type General Name', 'cpl_domain_cpr' ),
		'singular_name'         => _x( 'E-Resource', 'Post Type Singular Name', 'cpl_domain_cpr' ),
		'menu_name'             => __( 'E-Resource', 'cpl_domain_cpr' ),
		'name_admin_bar'        => __( 'E-Resource', 'cpl_domain_cpr' ),
		'archives'              => __( 'Item Archives', 'cpl_domain_cpr' ),
		'attributes'            => __( 'E-Resource Attributes', 'cpl_domain_cpr' ),
		'parent_item_colon'     => __( 'Parent Item:', 'cpl_domain_cpr' ),
		'all_items'             => __( 'All E-Resources', 'cpl_domain_cpr' ),
		'add_new_item'          => __( 'Add New Resource', 'cpl_domain_cpr' ),
		'add_new'               => __( 'Add New', 'cpl_domain_cpr' ),
		'new_item'              => __( 'New Resource', 'cpl_domain_cpr' ),
		'edit_item'             => __( 'Edit Resource', 'cpl_domain_cpr' ),
		'update_item'           => __( 'Update Resource', 'cpl_domain_cpr' ),
		'view_item'             => __( 'View Resource', 'cpl_domain_cpr' ),
		'view_items'            => __( 'View Resources', 'cpl_domain_cpr' ),
		'search_items'          => __( 'Search Resources', 'cpl_domain_cpr' ),
		'not_found'             => __( 'Not found', 'cpl_domain_cpr' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'cpl_domain_cpr' ),
		'featured_image'        => __( 'Featured Image', 'cpl_domain_cpr' ),
		'set_featured_image'    => __( 'Set featured image', 'cpl_domain_cpr' ),
		'remove_featured_image' => __( 'Remove featured image', 'cpl_domain_cpr' ),
		'use_featured_image'    => __( 'Use as featured image', 'cpl_domain_cpr' ),
		'insert_into_item'      => __( 'Insert into item', 'cpl_domain_cpr' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'cpl_domain_cpr' ),
		'items_list'            => __( 'E-Resources list', 'cpl_domain_cpr' ),
		'items_list_navigation' => __( 'E-Resources list navigation', 'cpl_domain_cpr' ),
		'filter_items_list'     => __( 'Filter E-Resources list', 'cpl_domain_cpr' ),
	);
	$args   = array(
		'label'               => __( 'E-Resource', 'cpl_domain_cpr' ),
		'description'         => __( 'An Electronic Resource', 'cpl_domain_cpr' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'excerpt', 'custom-fields', 'page-attributes' ),
		'taxonomies'          => array( 'cpl_resource_subject' ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'show_in_rest'        => true,
	);
	register_post_type( 'cpl_resource', $args );

}
