<?php

namespace cpl\custom_post_resources;


// name RESOURCE SUBJECT
// slug (for urls)
// official name: cpl_resource_subject
// type catgory, hierarchical
// url: resource-subject
// Register Custom Taxonomy
function register_cpl_resource_subject() {

	$labels  = array(
		'name'                       => _x( 'Resource Subjects', 'Taxonomy General Name', 'cpl_domain_cpr' ),
		'singular_name'              => _x( 'Resource Subject', 'Taxonomy Singular Name', 'cpl_domain_cpr' ),
		'menu_name'                  => __( 'Resource Subject', 'cpl_domain_cpr' ),
		'all_items'                  => __( 'All Resource Subjects', 'cpl_domain_cpr' ),
		'parent_item'                => __( 'Parent Resource Subject', 'cpl_domain_cpr' ),
		'parent_item_colon'          => __( 'Parent Resource Subject:', 'cpl_domain_cpr' ),
		'new_item_name'              => __( 'New Resource Subject Name', 'cpl_domain_cpr' ),
		'add_new_item'               => __( 'Add New Resource Subject', 'cpl_domain_cpr' ),
		'edit_item'                  => __( 'Edit Resource Subject', 'cpl_domain_cpr' ),
		'update_item'                => __( 'Update Resource Subject', 'cpl_domain_cpr' ),
		'view_item'                  => __( 'View Resource Subject', 'cpl_domain_cpr' ),
		'separate_items_with_commas' => __( 'Separate Resource Subjects with commas', 'cpl_domain_cpr' ),
		'add_or_remove_items'        => __( 'Add or remove Resource Subjects', 'cpl_domain_cpr' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'cpl_domain_cpr' ),
		'popular_items'              => __( 'Popular Resource Subject', 'cpl_domain_cpr' ),
		'search_items'               => __( 'Search Resource Subjects', 'cpl_domain_cpr' ),
		'not_found'                  => __( 'Not Found', 'cpl_domain_cpr' ),
		'no_terms'                   => __( 'No Resource Subjects', 'cpl_domain_cpr' ),
		'items_list'                 => __( 'Resource Subjects list', 'cpl_domain_cpr' ),
		'items_list_navigation'      => __( 'Items list navigation', 'cpl_domain_cpr' ),
	);
	$rewrite = array(
		'slug'         => 'resource-subject',
		'with_front'   => true,
		'hierarchical' => true,
	);
	$args    = array(
		'labels'            => $labels,
		'hierarchical'      => true,
		'public'            => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'show_tagcloud'     => true,
		'rewrite'           => $rewrite,
		'show_in_rest'      => true,
	);
	register_taxonomy( 'cpl_resource_subject', array( 'cpl_resource' ), $args );

}
